﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bucket : MonoBehaviour {

	public Transform playerPos;
	public Transform bucketPos;
	public GameObject iceBarrier1;
	public GameObject iceBarrier2;
	public GameObject blocker;

	void OnTriggerStay2D (Collider2D other)

	{
		if (other.tag == "Player") 
		{
			if (Input.GetKeyDown (KeyCode.Z))
			{
				playerPos.position = bucketPos.position;

				bucketPos.parent = playerPos;
			}
		}
		if (other.tag == "IceBarrier") 
		{
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				iceBarrier1.SetActive (false);
				iceBarrier2.SetActive (false);
				this.gameObject.SetActive (false);
				blocker.SetActive (false);

			}
		}

	}
}