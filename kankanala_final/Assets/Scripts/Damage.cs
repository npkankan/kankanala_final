﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

	public GameObject player;

	void OnTriggerStay2D(Collider2D other) 
	{
		if (other.tag == "Player") 
		{
			player.transform.position = new Vector2(0f, 0f);
			SimplePlatformController simplePlatformController = other.GetComponent<SimplePlatformController> ();
			simplePlatformController.LoseLife ();
		}
	}
}
