﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorToLevel3 : MonoBehaviour {

	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player")

		{
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				SceneManager.LoadScene ("Victory");
			}
		}
	}
}
