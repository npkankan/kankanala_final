﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireStone : MonoBehaviour {
	public Transform playerPos;
	public Transform stonePos;
	public Transform totemPos;
	public GameObject LockedGate;
	void OnTriggerStay2D (Collider2D other)

	{
		if (other.tag == "Player") 
		{
			if (Input.GetKeyDown (KeyCode.Z))
			{
				playerPos.position = stonePos.position;

				stonePos.parent = playerPos;
			}
		}
		if (other.tag == "FireStoneTotem") 
		{
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				stonePos.position = totemPos.position;

				stonePos.parent = totemPos;

				LockedGate.SetActive (false);

			}
		}

	}
}
