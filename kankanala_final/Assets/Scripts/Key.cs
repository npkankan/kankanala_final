﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour 
{
	bool isDoorOpen = false;
	public GameObject LockedDoor;
	public GameObject Key1;
	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player")

		{
			isDoorOpen = true;
			Debug.Log ("Door is Unlocked");
			LockedDoor.SetActive (false);
			this.gameObject.SetActive (false);
		}
	}
}
