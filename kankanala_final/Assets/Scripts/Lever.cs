﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour 
{

	bool arePlatformsVisible = false;
	public GameObject LeverPlatforms;

	// When the player presses Z on the lever,
	// the door is opened (in the engine it is destroyed)

	void Start()
	{
		LeverPlatforms.SetActive (false);
	}
	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player") 
		{
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				arePlatformsVisible = true;
				Debug.Log ("Platforms are visible!");
				LeverPlatforms.SetActive (true);
			}
		}
	}
		


}



