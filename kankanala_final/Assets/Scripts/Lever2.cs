﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever2 : MonoBehaviour

{

	bool arePlatformsVisible = false;
	public GameObject platform1;
	public GameObject platform2;
	public GameObject platform3;
	public GameObject platform4;
	public GameObject platform5;

	// When the player presses Z on the lever,
	// the door is opened (in the engine it is destroyed)

	void Start()
	{
		platform1.SetActive (false);
		platform2.SetActive (false);
		platform3.SetActive (false);
		platform4.SetActive (false);
		platform5.SetActive (false);
	}
	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player") 
		{
			if (Input.GetKeyDown (KeyCode.Z)) 
			{
				arePlatformsVisible = true;
				Debug.Log ("Platforms are visible!");
				platform1.SetActive (true);
				platform2.SetActive (true);
				platform3.SetActive (true);
				platform4.SetActive (true);
				platform5.SetActive (true);
			}
		}
	}



}


