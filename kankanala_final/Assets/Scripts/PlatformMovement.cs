﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour {

	public Transform platform;
	public Transform startTransform;
	public Transform endTransform;
	public float platformSpeed;

	private Vector3 direction;
	private Transform destination;

	void OnDrawGizmos()
	{
		Gizmos.DrawWireCube (startTransform.position, platform.localScale);
		Gizmos.DrawWireCube (endTransform.position, platform.localScale);
	}

	void FixedUpdate()
	{
		platform.GetComponent<Rigidbody2D>().MovePosition(platform.position + direction * platformSpeed * Time.fixedDeltaTime);
		if (Vector3.Distance (platform.position, destination.position) < platformSpeed * Time.fixedDeltaTime) {
			SetDestination (destination == startTransform ? endTransform : startTransform);
		}
	}

	void SetDestination(Transform dest)
	{
		destination = dest;
		direction = (destination.position - platform.position).normalized;

	}

	void Start()
	{
		SetDestination (startTransform);
	}
}


