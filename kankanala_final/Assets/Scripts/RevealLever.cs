﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevealLever : MonoBehaviour {

	public GameObject lever;
	// Use this for initialization
	void Start () {
		lever.transform.position = new Vector2(-41f, -48f);
	}


	void OnTriggerStay2D (Collider2D other)
	{
		if (other.tag == "Player") 
		{
			lever.transform.position = new Vector2 (-2.17f, -48.19f);
		}
	}
}
	
