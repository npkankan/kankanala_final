﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

	bool areIcyclesVisible = false;
	public GameObject Icycle1;
	public GameObject Icycle2;

	// When the player presses Z on the lever,
	// the door is opened (in the engine it is destroyed)

	void Start()
	{
		Icycle1.SetActive(false);
		Icycle2.SetActive (false);
	}
	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Player") 
		{
			
				areIcyclesVisible = true;
				Debug.Log ("Icycles are visible");
				Icycle1.SetActive (true);
				Icycle2.SetActive (true);
				this.gameObject.SetActive (false);

		}
	}



}